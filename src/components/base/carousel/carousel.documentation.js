import description from './carousel.md';

export default {
  description,
  bootstrapComponent: 'b-carousel',
};
