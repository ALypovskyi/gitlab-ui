import description from './tabs.md';

export default {
  description,
  followsDesignSystem: true,
};
