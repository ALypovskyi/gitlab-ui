import * as description from './form_text.md';

export default {
  description,
  bootstrapComponent: 'b-form-text',
  propsInfo: {},
};
